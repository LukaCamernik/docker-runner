package main

import (
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"os/exec"
	"strings"
	"time"
)

type Configuration struct {
	LogFile string
	Folders []string
}

var configuration Configuration
var printHelp bool

type arrayFlags []string

func (i *arrayFlags) String() string {
	return ""
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, strings.TrimSpace(value))
	return nil
}

func init() {
	var folders arrayFlags
	var logFile string
	flag.Var(&folders, "f", "Which folders/files to watch, you can specify "+
		"multiple by calling same flag multiple times (Required)")
	flag.StringVar(&logFile, "l", "", "Where to write the log file to, defaults to console output if empty")
	flag.BoolVar(&printHelp, "help", false, "Print this help message.")
	flag.Parse()
	configuration.Folders = folders
	configuration.LogFile = logFile

	if configuration.LogFile == "" {
		log.SetOutput(os.Stdout)
	} else {
		f, err := os.OpenFile(configuration.LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			log.Fatalf("error opening file: %v", err)
		}
		log.SetOutput(f)
	}
	log.SetLevel(log.DebugLevel)

	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	log.SetFormatter(customFormatter)
}

func checkRequiredParams() bool {
	if len(configuration.Folders) == 0 {
		fmt.Println("-----------------------------")
		flag.PrintDefaults()
		fmt.Println("-----------------------------")
		return false
	}
	return true
}

func main() {
	if !checkRequiredParams() {
		log.Info("[APP] Missing required parameters, exiting!")
		os.Exit(0)
	}
	log.Info("[APP] Docker Runner is running, waiting for mounted folders!")

	delay := time.Second
	for {
		if checkPaths() {
			runDocker()
			log.Info("[APP] Everything looks OK, exiting")
			os.Exit(0)
		}
		log.Warn(fmt.Sprintf("[APP] Folder/File did not exist, waiting for %s", delay.String()))
		// Adds 20ms to each execution attempt
		delay = time.Second + delay
		time.Sleep(delay)
	}
}

func pathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

func checkPaths() bool {
	folders := configuration.Folders
	for _, folder := range folders {
		log.Info(fmt.Sprintf("[FOLDER] Checking folder/file %s", folder))
		var path, err = pathExists(folder)
		if err != nil || !path {
			log.Warn(fmt.Sprintf("[FOLDER] folder/file %s doesn't exist", folder))
			return false
		}
	}
	log.Info("[FOLDER] All folders/files checked exist")
	return true
}

func runDocker() {
	// This will only run "docker ps" because this is what starts the service with least overhead
	c := exec.Command("docker", "ps")
	if err := c.Run(); err != nil {
		log.Error("[DOCKER] Docker is not starting")
	}
	log.Info("[DOCKER] Docker started running")
}
