If you start docker as service (systemd) then it doesn't wait for external disks to be mounted,
causing docker to have empty folders instead of real links.

To remedy that you need to remove start up sequence and start docker with any command (`docker ps`),
this just waits for the folders to become available and runs the command

To run it you can point it to folders (-f) like `docker-runner -f /mnt/disk1 -f /mnt/disk2`

You can also make it write to log file (-l) (make sure it's writable by the running user) `docker-runner -l /var/log/docker-runner.log`